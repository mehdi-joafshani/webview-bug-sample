package com.joe.sample;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.Toast;

public class CustomWebView extends WebView {

    private ActionMode mActionMode;
    private ActionMode.Callback mActionModeCallback;

    public CustomWebView(Context context) {
        super(context);
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean showContextMenu() {
        Log.d("JOE", "custom webview showContextMenu stacktrace: " + new Throwable());
        return super.showContextMenu();
    }

    @Override
    public ActionMode startActionMode(ActionMode.Callback callback) {
        Log.d("JOE", "startActionMode()");

        ViewParent parent = getParent();
        if (parent == null) {
            return null;
        }

        mActionModeCallback = new CustomActionModeCallback();

        mActionMode = parent.startActionModeForChild(this, mActionModeCallback);
        return mActionMode;
    }


    private class CustomActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Text selection");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            Toast.makeText(CustomWebView.this.getContext(), "I need to show my custom menu panel now !", Toast.LENGTH_LONG).show();
            return false; // Return false if nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            mode.finish();
            clearFocus();
        }
    }


}
